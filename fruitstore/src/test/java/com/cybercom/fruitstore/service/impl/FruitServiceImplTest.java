package com.cybercom.fruitstore.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.cybercom.fruitstore.dao.FruitDAO;
import com.cybercom.fruitstore.domain.Fruit;
import com.cybercom.fruitstore.dto.FruitDTO;
import com.cybercom.fruitstore.service.FruitService;

@RunWith(SpringRunner.class)
public class FruitServiceImplTest {

	@TestConfiguration
	static class FruitServiceImplTestContextConfiguration {

		@Bean
		public FruitService employeeService() {
			return new FruitServiceImpl();
		}
	}

	@Autowired
	private FruitService fruitService;

	@MockBean
	private FruitDAO fruitDAO;
	
	@Before
	public void setUp() {
		
		Fruit fruit = new Fruit();
		
		fruit.setId(1);
		fruit.setName("Banana");
		fruit.setType("A-Class");
		fruit.setPrice(15);
		
	    Optional<Fruit> banana = Optional.of(fruit);
	 
	    Mockito.when(fruitDAO.findById(banana.get().getId()))
	      .thenReturn(banana);
	    
	    Mockito.when(fruitDAO.save(fruit))
	      .thenReturn(fruit);
	}
	
	@Test
	public void testFindFruitById() {
		FruitDTO fruitDTO = fruitService.findFruitById(1);
		assertThat(fruitDTO.getName()).isEqualTo("Banana");
	}
	
	@Test
	public void testSaveFruit() {
		
		FruitDTO fruitDTO = getFruitDTO();
		
		FruitDTO savedFruitDTO = fruitService.saveFruit(fruitDTO);
		
		assertThat(savedFruitDTO.getName()).isEqualTo("Banana");
	}
	
	@Test
	public void testUpdateFruit() {
		
		FruitDTO fruitDTO = getFruitDTO();
		
		FruitDTO savedFruitDTO = fruitService.updateFruit(fruitDTO, 1);
		
		assertThat(savedFruitDTO.getName()).isEqualTo("Banana");
		
	}
	
	/**
	 * Provide FruitDTO object
	 * 
	 * @return fruitDTO object
	 */
	public static FruitDTO getFruitDTO() {
		
		FruitDTO fruitDTO = new FruitDTO();
		
		fruitDTO.setId(1);
		fruitDTO.setName("Banana");
		fruitDTO.setType("A-Class");
		fruitDTO.setPrice(15);
		
		return fruitDTO;
	}
}
