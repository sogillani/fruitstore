package com.cybercom.fruitstore.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.cybercom.fruitstore.domain.Fruit;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FruitDAOTest {
	
	@Autowired
    private TestEntityManager entityManager;
	
	@Autowired
	private FruitDAO fruitDAO;
	
	@Test
	public void testFindById() {
		
		Fruit fruit = new Fruit();
		fruit.setId(1);
		fruit.setName("Banana");
		fruit.setType("A-Class");
		fruit.setPrice(15);
		
		entityManager.persist(fruit);
		entityManager.flush();
		
		Optional<Fruit> found = fruitDAO.findById(1);
		
		assertThat(found.isPresent());
		assertThat(found.get().getName().equals("Banana"));
	}

}
