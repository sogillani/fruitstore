package com.cybercom.fruitstore.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.cybercom.fruitstore.dto.FruitDTO;
import com.cybercom.fruitstore.service.FruitService;
import com.cybercom.fruitstore.service.impl.FruitServiceImplTest;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FruitController.class)
public class FruitControllerTest {
	
    @Autowired
    private MockMvc mvc;
    
    @MockBean
    private FruitService fruitService;
    
    @Test
    public void testGetFruit() throws Exception {
    	
    	// Reused the same DTO object to save time
    	FruitDTO fruitDTO = FruitServiceImplTest.getFruitDTO();
		
		when(fruitService.findFruitById(1)).thenReturn(fruitDTO);
		
		 mvc.perform(get("/fruitstore/fruit/1")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.name", is(fruitDTO.getName())));
    	
    }
    
    @Test
    public void testsaveFruit() throws Exception {
    	
    	// Reused the same DTO object to save time
    	FruitDTO fruitDTO = FruitServiceImplTest.getFruitDTO();
		
		when(fruitService.saveFruit(fruitDTO)).thenReturn(fruitDTO);
		
		String fruitDTOJSON = "{\"id\": \"1\", \"type\": \"Type-A\", \"name\": \"Banana\", \"price\": \"15\"}";
		
		mvc.perform(post("/fruitstore/fruit")
				  .content(fruitDTOJSON.getBytes())
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(status().isOk())
			      .andExpect(jsonPath("$.name", is(fruitDTO.getName())));
    	
    }

}
