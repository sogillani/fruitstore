package com.cybercom.fruitstore.dto;

/**
 * @author Gillani
 *
 */
public class FruitDTO {

	private int id;
	private String type;
	private String name;
	private int price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == this)
			return true;
		
		if(!(obj instanceof FruitDTO))
			return false;
		
		FruitDTO fruitObj = (FruitDTO) obj;
		
		if (fruitObj.getId() == this.id)
			return true;
		
		return false;
	}
}
