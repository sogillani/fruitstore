package com.cybercom.fruitstore.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybercom.fruitstore.dto.FruitDTO;
import com.cybercom.fruitstore.service.FruitService;

/**
 * 
 * @author Omer Gillani
 *
 */
@RestController
@RequestMapping(path = "/fruitstore")
public class FruitController {

	@Autowired
	private FruitService fruitService;

	@GetMapping(path = "/fruit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<FruitDTO> getFruit(@PathVariable("id") Integer id) {
		
		FruitDTO fruitDTO = fruitService.findFruitById(id);
		if (fruitDTO == null) {
			return new ResponseEntity<FruitDTO>(HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok(fruitDTO);
	}

	@PostMapping(path = "/fruit"/*, consumes = MediaType.APPLICATION_JSON_VALUE*/)
	public ResponseEntity<FruitDTO> saveFruit(@RequestBody FruitDTO fruitDTO) {
		FruitDTO savedFruitDTO = fruitService.saveFruit(fruitDTO);
		return ResponseEntity.ok(savedFruitDTO);
	}

	@PutMapping(path = "/fruit/{id}"/*, consumes = MediaType.APPLICATION_JSON_VALUE*/)
	public ResponseEntity<FruitDTO> updateFruit(@RequestBody FruitDTO fruitDTO, @PathVariable("id") Integer id) {
		
		FruitDTO updatedFruit = fruitService.updateFruit(fruitDTO, id);
		
		if (updatedFruit == null) {
			return new ResponseEntity<FruitDTO>(HttpStatus.NOT_FOUND);
		}
		
		return ResponseEntity.ok(updatedFruit);
	}
	
	@GetMapping(path = "/fruit/")
	public ResponseEntity<List<FruitDTO>> listAllFruits() {
		List<FruitDTO> fruitDTOs = fruitService.findAllFruits();
		
		if (fruitDTOs.isEmpty()) {
			return new ResponseEntity<List<FruitDTO>>(HttpStatus.NO_CONTENT);
		}
		
		return ResponseEntity.ok(fruitDTOs);
	}
}