package com.cybercom.fruitstore.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cybercom.fruitstore.domain.Fruit;

/**
 * 
 * @author Omer Gillani
 *
 */

@Repository
public interface FruitDAO extends JpaRepository<Fruit,Integer> {

}