package com.cybercom.fruitstore.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cybercom.fruitstore.dto.FruitDTO;
import com.cybercom.fruitstore.service.FruitService;
import com.google.gson.Gson;


@Component
public class FruitMessages implements MqttCallback {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FruitMessages.class);

	@Autowired
	private FruitService fruitService;

	MqttClient mqtt;

	public FruitMessages() throws MqttException {
		this.mqtt = new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
		mqtt.setCallback(this);
		mqtt.connect();
		mqtt.subscribe("new/fruit");
	}

	@Override
	public void connectionLost(Throwable cause) {
		LOGGER.error("MQTT connection lost", cause);
	}

	@Override
	public void messageArrived(String topic, MqttMessage message) throws Exception {
		Gson gson = new Gson();
		FruitDTO fruitDTO = gson.fromJson(new String(message.getPayload()), FruitDTO.class);
		fruitService.saveFruit(fruitDTO);
		LOGGER.debug(new String(message.getPayload()));
	}

	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
	}

}