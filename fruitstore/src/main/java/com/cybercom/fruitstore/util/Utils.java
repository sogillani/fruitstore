package com.cybercom.fruitstore.util;

import com.cybercom.fruitstore.domain.Fruit;

/**
 * 
 * @author Omer Gillani
 *
 */
public class Utils {

	/**
	 * Return true if type of fruit is banana
	 * 
	 * @param fruit
	 * @return
	 */
	static boolean isBanana(Fruit fruit) {
		if ("Banana".equalsIgnoreCase(fruit.getType()))
			return true;
		
		return false;
	}
}
