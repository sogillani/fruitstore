package com.cybercom.fruitstore.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybercom.fruitstore.dao.FruitDAO;
import com.cybercom.fruitstore.domain.Fruit;
import com.cybercom.fruitstore.dto.FruitDTO;
import com.cybercom.fruitstore.service.FruitService;

/**
 * 
 * @author Omer Gillani
 *
 */
@Service
public class FruitServiceImpl implements FruitService {
	
	@Autowired
	FruitDAO fruitDAO;

	@Override
	public FruitDTO findFruitById(Integer id) {
		
		Optional<Fruit> fruitOptional = fruitDAO.findById(id);
		
		if (!fruitOptional.isPresent()) {
			return null;
		}
		
		Fruit fruit = fruitOptional.get();
		FruitDTO fruitDTO = new FruitDTO();
		BeanUtils.copyProperties(fruit, fruitDTO);
		
		return fruitDTO;
	}

	@Override
	public FruitDTO saveFruit(FruitDTO fruitDTO) {
		
		Fruit fruit = new Fruit();
		BeanUtils.copyProperties(fruitDTO, fruit);
		Fruit savedFruit = fruitDAO.save(fruit);
		
		FruitDTO savedFruitDTO = new FruitDTO();
		BeanUtils.copyProperties(savedFruit, savedFruitDTO);
		
		return savedFruitDTO;
	}

	@Override
	public FruitDTO updateFruit(FruitDTO fruitDTO, Integer id) {
		
		Optional<Fruit> fruitOptional = fruitDAO.findById(id);
		
		if (!fruitOptional.isPresent())
			return null;
		
		Fruit fruit = fruitOptional.get();
		
		BeanUtils.copyProperties(fruitDTO, fruit);
		Fruit savedFruit = fruitDAO.save(fruit);
		
		FruitDTO savedFruitDTO = new FruitDTO();
		BeanUtils.copyProperties(savedFruit, savedFruitDTO);
		
		return savedFruitDTO;
	}

	@Override
	public List<FruitDTO> findAllFruits() {
		
		List<FruitDTO> fruitDTOs = new ArrayList<>();
		List<Fruit> fruits = fruitDAO.findAll();
		
		for (Fruit fruit : fruits) {
			FruitDTO fruitDTO = new FruitDTO();
			BeanUtils.copyProperties(fruit, fruitDTO);
			fruitDTOs.add(fruitDTO);
		}
		
		return fruitDTOs;
	}
}
