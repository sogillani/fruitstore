package com.cybercom.fruitstore.service;

import java.util.List;

import com.cybercom.fruitstore.dto.FruitDTO;

/**
 * 
 * @author Omer Gillani
 *
 */
public interface FruitService {
	FruitDTO findFruitById(Integer id);
	FruitDTO saveFruit(FruitDTO fruitDTO);
	FruitDTO updateFruit(FruitDTO fruitDTO, Integer id);
	List<FruitDTO> findAllFruits();
}
