package com.cybercom.fruitstore.domain;

import java.util.Random;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * @author Omer Gillani
 *
 */

@Entity
public class Fruit {

	@Id
	private int id;
	private String type;
	private String name;
	private int price;

	public Fruit() {

	}

	/**
	 * 
	 * @param type
	 * @param name
	 * @param price
	 * @param id
	 */
	public Fruit(String type, String name, int price, int id) {
		if (id == 0) {
			Random rand = new Random();
			this.id = rand.nextInt();
		} else {
			this.id = id;
		}

		setType(type);
		setPrice(price);
		setName(name);
	}

	/**
	 * 
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param t
	 *            the t to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the p to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param n
	 *            the n to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj == this)
			return true;
		
		if(!(obj instanceof Fruit))
			return false;
		
		Fruit fruitObj = (Fruit) obj;
		
		if (fruitObj.getId() == this.id)
			return true;
		
		return false;
	}
}