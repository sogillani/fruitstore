
REM Maven Build
call mvn clean install

REM Run spring-boot
REM mvn spring-boot:run

REM Create Docker image
docker build -t product-api .

REM Run an image
docker run -it -d -p 8080:8080 --name product-api product-api

