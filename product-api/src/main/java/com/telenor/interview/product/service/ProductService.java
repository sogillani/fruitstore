package com.telenor.interview.product.service;

import com.telenor.interview.product.dto.ProductDto;

import java.util.List;

public interface ProductService {
    List<ProductDto> findProductsByFilters(String type,
                                           Double minPrice,
                                           Double maxPrice,
                                           String city,
                                           String property,
                                           String color,
                                           Double gbLimitMin,
                                           Double gbLimitMax);
}
