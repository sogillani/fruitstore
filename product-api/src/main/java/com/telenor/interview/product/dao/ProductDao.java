package com.telenor.interview.product.dao;

import com.telenor.interview.product.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<Product, Long>,
        JpaSpecificationExecutor<Product> {
}
