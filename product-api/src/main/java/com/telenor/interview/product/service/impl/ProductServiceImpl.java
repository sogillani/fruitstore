package com.telenor.interview.product.service.impl;

import com.telenor.interview.product.dao.ProductDao;
import com.telenor.interview.product.dto.ProductDto;
import com.telenor.interview.product.model.Product;
import com.telenor.interview.product.model.ProductSpecification;
import com.telenor.interview.product.service.ProductService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDao productDao;

    @Override
    public List<ProductDto> findProductsByFilters(String type,
                                                  Double minPrice,
                                                  Double maxPrice,
                                                  String city,
                                                  String property,
                                                  String color,
                                                  Double gbLimitMin,
                                                  Double gbLimitMax) {

        ProductSpecification specification = new ProductSpecification();

        if (type != null) {
            specification.addSpecification(ProductSpecification.typeFilter(type));
        }

        if (minPrice != null) {
            specification.addSpecification(ProductSpecification.minPriceFilter(minPrice));
        }

        if (maxPrice != null) {
            specification.addSpecification(ProductSpecification.maxPriceFilter(maxPrice));
        }

        if (city != null) {
            specification.addSpecification(ProductSpecification.cityFilter(city));
        }

        if (property != null) {
            specification.addSpecification(ProductSpecification.propertyFilter(property));
        }

        if (color != null) {
            specification.addSpecification(ProductSpecification.colorFilter(color));
        }

        if (gbLimitMin != null) {
            specification.addSpecification(ProductSpecification.gbLimitMinFilter(gbLimitMin));
        }

        if (gbLimitMax != null) {
            specification.addSpecification(ProductSpecification.gbLimitMaxFilter(gbLimitMax));
        }

        List<Product> products = productDao.findAll(specification);
        return map(products);
    }

    private List<ProductDto> map(List<Product> products) {
        return products.stream()
                .map(product -> {
                    ProductDto productDto = new ProductDto();
                    BeanUtils.copyProperties(product, productDto);
                    return productDto;
                }).collect(Collectors.toList());
    }
}
