package com.telenor.interview.product.model;

import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class serve two purpose
 * 1. Filter the data based on specification filters
 * 2. This class provides static factory methods
 *    to create different kind of filters required for products filtering
 */
@ToString
public class ProductSpecification implements Specification<Product> {

    public static final Logger LOGGER = LoggerFactory.getLogger(ProductSpecification.class);

    private final List<Specification<Product>> specifications = new ArrayList<>();

    /**
     * Add specifications filters
     *
     * @param specification for filtering data
     */
    public void addSpecification(Specification<Product> specification) {
        specifications.add(specification);
    }

    @Override
    public Predicate toPredicate(Root<Product> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        LOGGER.debug("Specifications list size: {}", specifications.size());

        List<Predicate> predicates = specifications.stream()
                .map(specification -> specification.toPredicate(root, criteriaQuery, criteriaBuilder))
                .collect(Collectors.toList());

        LOGGER.debug("Predicate list size: {}", predicates.size());
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    /**
     * Factory method for type specification lambda
     *
     * @param type type of product
     * @return Specification filter for product type
     */
    public static Specification<Product> typeFilter(String type) {
        return (root, query, cb) -> cb.equal(root.get("type"), type);
    }

    /**
     * Factory method for minimum price specification lambda
     *
     * @param minPrice minimum price of product
     * @return Specification filter for minimum price
     */
    public static Specification<Product> minPriceFilter(Double minPrice) {
        return (root, query, cb) -> cb.greaterThanOrEqualTo(root.get("price"), minPrice);
    }

    /**
     * Factory method for maximum price specification lambda
     *
     * @param maxPrice maximum price of product
     * @return Specification filter for maximum price
     */
    public static Specification<Product> maxPriceFilter(Double maxPrice) {
        return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("price"), maxPrice);
    }

    /**
     * Factory method for city specification lambda
     *
     * @param city product city
     * @return Specification filter for city
     */
    public static Specification<Product> cityFilter(String city) {
        return (root, query, cb) -> cb.like(
                cb.lower(root.get("storeAddress")),
                "%" + city.toLowerCase() + "%");
    }

    /**
     * Factory method for property specification lambda
     *
     * @param property property name
     * @return Specification filter for property
     */
    public static Specification<Product> propertyFilter(String property) {
        return (root, query, cb) -> cb.like(root.get("properties"), property + ":%");
    }

    /**
     * Factory method for color specification lambda
     *
     * @param color color of product
     * @return Specification filter for color
     */
    public static Specification<Product> colorFilter(String color) {
        return (root, query, cb) -> cb.like(root.get("properties"), "color:" + color);
    }

    /**
     * Factory method for minimum GB limit specification lambda
     *
     * @param gbLimitMin minimum GB limit
     * @return Specification filter for minimum GB limit
     */
    public static Specification<Product> gbLimitMinFilter(Double gbLimitMin) {
        Specification<Product> gbLimitMinSpec = (root, query, cb) -> {
            Expression<Double> subStr = cb.substring(root.get("properties"), 10).as(Double.class);
             return cb.greaterThanOrEqualTo(subStr, gbLimitMin);
        };
        return propertyFilter("gb_limit").and(gbLimitMinSpec);
    }

    /**
     * Factory method for maximum GB limit
     *
     * @param gbLimitMax maximum GB limit
     * @return Specification filter for maximum GB limit
     */
    public static Specification<Product> gbLimitMaxFilter(Double gbLimitMax) {
        Specification<Product> gbLimitMaxSpec = (root, query, cb) -> {
            Expression<Double> subStr = cb.substring(root.get("properties"), 10).as(Double.class);
            return cb.lessThanOrEqualTo(subStr, gbLimitMax);
        };
        return propertyFilter("gb_limit").and(gbLimitMaxSpec);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductSpecification that = (ProductSpecification) o;

        // Considering equal if size of specification match
        // Required for mockito ProductServiceImplTest#testFindProductsByFilters unit test
        return specifications.size() == that.specifications.size();
    }

    @Override
    public int hashCode() {
        return Objects.hash(specifications);
    }
}
