package com.telenor.interview.product.controller;

import com.telenor.interview.product.dto.ProductDto;
import com.telenor.interview.product.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@RestController
public class ProductController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;

    @GetMapping(path = "/product/")
    public ResponseEntity<Map<String, List<ProductDto>>> getProducts(
            @RequestParam(required = false) String type,
            @RequestParam(required = false, name = "min_price") Double minPrice,
            @RequestParam(required = false, name = "max_price") Double maxPrice,
            @RequestParam(required = false, name = "city") String city,
            @RequestParam(required = false, name = "property") String property,
            @RequestParam(required = false, name = "property:color") String color,
            @RequestParam(required = false, name = "property:gb_limit_min") Double gbLimitMin,
            @RequestParam(required = false, name = "property:gb_limit_max") Double gbLimitMax) {

        List<ProductDto> productDtoList = productService.findProductsByFilters(type,
                minPrice, maxPrice, city, property, color, gbLimitMin, gbLimitMax);

        LOGGER.debug("Product List Size: {}", productDtoList.size());

        // Adding "data" wrapper on products list
        Map<String, List<ProductDto>> wrapperMap = Collections.singletonMap("data", productDtoList);
        return ResponseEntity.ok(wrapperMap);
    }
}
