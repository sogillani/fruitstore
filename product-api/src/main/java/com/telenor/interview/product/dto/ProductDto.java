package com.telenor.interview.product.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ProductDto {
    private String type;
    private String properties;
    private Double price;

    @JsonProperty("store_address")
    private String storeAddress;
}
