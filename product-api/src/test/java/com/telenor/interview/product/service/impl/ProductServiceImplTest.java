package com.telenor.interview.product.service.impl;

import com.telenor.interview.product.dao.ProductDao;
import com.telenor.interview.product.dto.ProductDto;
import com.telenor.interview.product.model.Product;
import com.telenor.interview.product.model.ProductSpecification;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {

    @InjectMocks
    private ProductServiceImpl productServiceImpl;

    @Mock
    private ProductDao productDao;

    @Before
    public void setUp() {
        when(productDao.findAll(new ProductSpecification()))
                .thenReturn(Lists.list(new Product()));

        ProductSpecification specification = new ProductSpecification();

        specification.addSpecification(ProductSpecification.typeFilter("phone"));
        specification.addSpecification(ProductSpecification.colorFilter("guld"));
        specification.addSpecification(ProductSpecification.propertyFilter("color"));
        specification.addSpecification(ProductSpecification.cityFilter("Stockholm"));
        specification.addSpecification(ProductSpecification.maxPriceFilter(200.0));
        specification.addSpecification(ProductSpecification.minPriceFilter(100.0));
        specification.addSpecification(ProductSpecification.gbLimitMaxFilter(50.0));
        specification.addSpecification(ProductSpecification.gbLimitMinFilter(10.0));

        when(productDao.findAll(specification)).thenReturn(Lists.list(new Product(), new Product()));
    }

    @Test
    public void testFindProductsByFilters() {
        List<ProductDto> productDtoList = productServiceImpl.findProductsByFilters(null, null, null, null, null, null, null, null);
        assertThat(productDtoList).hasSize(1);

        productDtoList = productServiceImpl.findProductsByFilters("phone", 100.0, 200.0, "Stockholm", "color", "guld", 10.0, 50.0);
        assertThat(productDtoList).hasSize(2);
    }
}
