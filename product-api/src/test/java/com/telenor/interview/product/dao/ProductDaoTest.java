package com.telenor.interview.product.dao;

import com.telenor.interview.product.model.Product;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductDaoTest {

    @Autowired
    private ProductDao productDao;

    @Test
    public void testFindAll() {
        List<Product> products = productDao.findAll();
        assertThat(products).hasSize(100);
    }
}
