package com.telenor.interview.product.model;

import com.telenor.interview.product.dao.ProductDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductSpecificationTest {

    @Autowired
    private ProductDao productDao;

    @Test
    public void testTypeFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.typeFilter("phone"));
        assertThat(products).hasSize(42);
    }

    @Test
    public void testMinPriceFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.minPriceFilter(200.0));
        assertThat(products).hasSize(78);
    }

    @Test
    public void testMaxPriceFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.maxPriceFilter(300.0));
        assertThat(products).hasSize(41);
    }

    @Test
    public void testCityFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.cityFilter("Stockholm"));
        assertThat(products).hasSize(40);
    }

    @Test
    public void testPropertyFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.propertyFilter("color"));
        assertThat(products).hasSize(42);
    }

    @Test
    public void testColorFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.colorFilter("guld"));
        assertThat(products).hasSize(7);
    }

    @Test
    public void testGbLimitMinFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.gbLimitMinFilter(11.0));
        assertThat(products).hasSize(34);
    }

    @Test
    public void testGbLimitMaxFilter() {
        List<Product> products = productDao.findAll(ProductSpecification.gbLimitMaxFilter(11.0));
        assertThat(products).hasSize(24);
    }

    @Test
    public void testToPredicate() {
        ProductSpecification specification = new ProductSpecification();

        specification.addSpecification(ProductSpecification.typeFilter("phone"));
        specification.addSpecification((ProductSpecification.colorFilter("guld")));

        List<Product> products = productDao.findAll(specification);
        assertThat(products).hasSize(7);

        assertNotNull(products.get(0).getId());
    }
}
